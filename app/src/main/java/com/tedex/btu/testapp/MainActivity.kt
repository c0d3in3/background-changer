package com.tedex.btu.testapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        screen.setTag(R.drawable.background_1)
    }

    fun myClickHandler(target: View) {
        val resId = screen.getTag() as Int
        if (resId == R.drawable.background_1 || resId == null) {
            screen.setBackgroundResource(R.drawable.background_2)
            screen.setTag(R.drawable.background_2)
        }
        if (resId == R.drawable.background_2) {
            screen.setBackgroundResource(R.drawable.background_1)
            screen.setTag(R.drawable.background_1)
        }
    }
}


